Rails.application.routes.draw do
  devise_for :users
  root "dashboards#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'session' => 'sessions#create', :as=> :session_enduser
  get 'signup' => 'customers#new',:as=> :signup
  get 'logout' => 'sessions#destroy', :as => :logout
  get 'login' => 'sessions#new', :as => :login
  get "dashboard" => 'dashboards#index', :as => :dashboard_page
end

module EndusersHelper
  def current_enduser
    @current_customer ||= Enduser.find(session[:enduser_id]) if session[:enduser_id]
  end

  def logged_in?
    current_enduser
  end

  def login_required
    unless logged_in?
      flash[:error] = "You must first log in or sign up before accessing this page."
      store_target_location
      redirect_to login_url
    end
  end

  def redirect_to_target_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end

  private

    def store_target_location
      session[:return_to] = request.url
    end
end

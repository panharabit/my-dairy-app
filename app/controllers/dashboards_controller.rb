class DashboardsController < ApplicationController
  # before_action :authenticate_user!
  # before_action :admin_only, only:[:new,:edit,:update,:destroy]

  def index
    # @cards = {
    #    "fa fa-user"=>"icon-blue-flat",
    #    "fa fa-folder-open"=>"icon-pink-flat",
    #    "fa fa-file-text-o"=>"icon-green-flat",
    #    "fa fa-pie-chart"=>"icon-lighred-flat",
    #    "fa fa-users"=>"icon-purple-flat",
    #    "fa fa-cog"=>"icon-orange-flat",
    #    "fa fa-calendar"=>"icon-strong-blue-flat"
    #  }

     @cards = {
       "Profile" => {
         "fa fa-user" => "card-blue-flat"
       },
       "Data" => {
         "fa fa-folder-open" => "card-pink-flat"
       },
       "Record" => {
         "fa fa-file-text-o" => "card-green-flat"
       },
       "Chart" => {
         "fa fa-pie-chart" => "card-lighred-flat"
       },
       "Users" => {
         "fa fa-users" => "card-purple-flat"
       },
       "Setting" => {
        "fa fa-cog" => "card-orange-flat"
       },
       "Calendar" => {
         "fa fa-calendar" => "card-strong-blue-flat"
       },
       "Calendar1" => {
         "fa fa-calendar" => "card-strong-blue-flat"
       }
     }
  end
end

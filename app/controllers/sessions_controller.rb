class SessionsController < ApplicationController
  def new
  end

def create
  enduser = Enduser.authenticate(params[:login],params[:password])
  if enduser
    session[:enduser_id] = enduser.id
    flash[:notice] = "Logged In"
    redirect_to ("/")
  else
    flash.now[:error] = "Invalid Login or Password"
    render :action => 'new'
  end
end

def destroy
  session[:enduser_id] =nil
  flash[:notice] = "You have been logout"
  redirect_to "/"
end

end

class EndUsersController < ApplicationController
  before_action :set_enduser, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only:[:destroy]
  before_action :admin_only, only:[:destroy]
  before_action :login_required
  before_action :current_enduser


  def index
    @customers = Enduser.all

  end

  def show
    @customer = Enduser.find(params[:id])

  end

  def new
    @customer = Enduser.new
  end

  def create
    @customer = Enduser.new(enduser_params)
    if @customer.save
      session[:enduser_id] = @customer.id
      flash[:notice] = "Thank For Sign Up"
      redirect_to :homes
    else
      render :action => 'new'
    end
  end


  def edit
    @customer = Enduser.find(params[:id])
  end

  def update
    @customer = Enduser.find(params[:id])
    if session[:enduser_id] == @customer.id
      if @customer.update(enduser_params)
        redirect_to logout_path
      else
        flash[:notice] = "All Fill Require"
        render :edit
      end
    else
      flash[:notice] = "Your Attempt To Hack This Men Account"
      render :edit
    end
  end

  def destroy
    @customer.destroy
    redirect_to customers_url, notic: "Customer Has Been Deleted"
  end

  private
    def set_enduser
      @customer = Enduser.find(params[:id])
    end

    def admin_only
      unless current_user.admin?
        redirect_to root_path, :alert => "Access denied."
      end
    end

    def enduser_params
      params.require(:enduser).permit(:username,:email,:password,:salt,:encrypted_password)
    end
end

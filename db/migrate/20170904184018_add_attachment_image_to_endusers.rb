class AddAttachmentImageToEndusers < ActiveRecord::Migration[5.1]
  def self.up
    change_table :endusers do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :endusers, :image
  end
end

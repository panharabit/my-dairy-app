class AddImageToEndusers < ActiveRecord::Migration[5.1]
  def change
    add_column :endusers, :image, :string
  end
end

class AddPasswordConfirmationToEndusers < ActiveRecord::Migration[5.1]
  def change
    add_column :endusers, :password_confirmation, :string
  end
end
